# ClickClickClick

[Demo](https://jaune.gitlab.io/clickclickclick/)

![preview](https://gitlab.com/Jaune/clickclickclick/raw/master/preview.png "Preview")

# TODO
- add milestones / achievements
- add feedback on generator when attack
- add bosses
- fix wave level width above lvl99
- balance gold / damage / generators / repair
