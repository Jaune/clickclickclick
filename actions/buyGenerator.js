export default function buyGenerator(identifier) {
  return {
    type: 'BUY_GENERATOR',
    identifier
  };
}
