# Changelog

## [1.1.0] - 2017-07-12
### Added
- Changelog
- Now creeps attack, loot and retreat
- Now, you can repair your castle
- Debounce click
- Add feedback on click
- Responsive actions !
### Removed
- c3
- inferno
### Changed
- Generators on top
