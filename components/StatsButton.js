import React, { PureComponent } from 'react';
import StatsPanel from './StatsPanel';
import style from './StatsButton.css';

export default class StatsButton extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    };
  }

  render() {
    return (
      <div className={style.container}>
        <button
          className={style.button}
          onClick={() => (this.setState(() => ({ isOpen: !this.state.isOpen })))}>Stats</button>
        {this.state.isOpen ? <StatsPanel /> : null}
      </div>
    );
  }
}
