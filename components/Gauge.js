import React from 'react';
import style from './Gauge.css';

export default function Gauge(props) {
  const p = 100 * props.value / props.max;

  return (
    <div className={style.gauge}>
      <div className={style.gaugeCursor} style={{ width: p + '%' }}>
        <div className={style.gaugeText}>{props.value} / {props.max}</div>
      </div>
    </div>
  );
}
