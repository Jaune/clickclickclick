import React, { PureComponent } from 'react';
import style from './Gold.css';

export default class Gold extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      value: props.value,
      modifiers: []
    };
  }

  componentWillMount() {
    setInterval(() => (this.cleanup()), 500);
  }

  componentWillReceiveProps(nextProps) {
    const value = nextProps.value;
    const oldValue = this.state.value;

    if (value === oldValue) {
      return;
    }

    const noiseX = Math.random();
    const noiseY = Math.random();

    const modifier = {
      value: value - this.state.value,
      ttl: 15,
      key: Math.round(noiseX * 0xff00ff).toString(36) + '-' + Math.round(noiseY * 0xff00ff).toString(36),
      noiseX,
      noiseY
    };

    const modifiers = [...this.state.modifiers, modifier];

    this.setState(() => ({ value, modifiers  }));
  }

  cleanup() {
    if (this.state.modifiers.length === 0) {
      return;
    }

    const modifiers = this.state.modifiers
      .map((m) => (Object.assign({}, m, { ttl: m.ttl - 1 })))
      .filter((m) => (m.ttl > 0))
    ;

    this.setState(() => ({ modifiers }));
  }

  render() {
    return (
      <div className={style.container}>
        <div className={style.modifiers}>
          {this.state.modifiers.map((modifier) => {
            const x = modifier.noiseX * 100;
            const y = modifier.noiseY * 100;
            const classes = [style.modifier];

            if (modifier.value < 0) {
              classes.push(style.negative);
            } else if (modifier.value > 0) {
              classes.push(style.positive);
            }

            return (
              <div
                key={modifier.key}
                className={classes.join(' ')}
                style={{ top: `${y}%`, left: `${x}%` }}
                >{modifier.value > 0 ? '+' : ''}{modifier.value}</div>
            );
          })}
        </div>
        <div className={style.goldImage}>Gold</div>
        <div className={style.goldValue}>{this.state.value}</div>
      </div>
    );
  }
}
