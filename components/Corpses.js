import React from 'react';
import style from './Corpses.css';

export default function Corpses(props) {
  return (
    <div className={style.corpses}>
      {props.corpses.map(function (corpse, index) {
        const d = corpse.distance / 1000;

        const a = (Math.PI * 2) * corpse.alpha;
        const r = 6.5 + (15 * d);

        const x = -0.5 + (Math.cos(a) * r);
        const y = -0.5 + (Math.sin(a) * r);

        const classes = [style.corpse];

        if (corpse.ttl < 2) {
          classes.push(style.decay);
        }

        return <div key={corpse.key} className={classes.join(' ')} style={{ transform: `translate(${x}rem, ${y}rem)` }} />;
      })}
    </div>
  );
}
