import React from 'react';
import computeCosts from '../lib/computeCosts';
import style from './Generator.css';
import GENERATORS from '../generators.js';

export default function Generator(props) {
  const costs = computeCosts(props.identifier, props.count);
  const GENERATOR = GENERATORS[props.identifier];

  return (
    <div className={style.container}>
      <div className={style.firstLine}>
        <div className={[style.image, style[props.identifier]].join(' ')}>
          <div className={style.tooltip}>
            <div className={style.attackPerTick}>{GENERATOR.hitPerAttack}</div>
            <div className={style.damagePerTick}>{GENERATOR.damagePerAttack}</div>
          </div>
        </div>
        <div className={style.count}>{props.count}</div>
      </div>

      <div className={style.buy}>
        <button
          type="button"
          className={style.button}
          onClick={props.onBuy}>Buy</button>
        <div className={style.costs}>
          <div className={[style.cost, style.gold].join(' ')}>{costs.gold}</div>
        </div>
      </div>
    </div>
  );
}
