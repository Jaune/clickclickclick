import React, { PureComponent } from 'react';
import style from './StatsPanel.css';

import c3 from 'c3';
import 'c3/c3.css';

const funcs = [

function (levelMax) {
  const data = ['level * level'];

  for (let level = 1; level <= levelMax; level++) {
    data.push(
      level * level
    );
  }

  return data;
},
function (levelMax) {
  const data = ['Math.round((level * level) / (1 + Math.log(level)))'];

  for (let level = 1; level <= levelMax; level++) {
    data.push(
      Math.round((level * level) / (1 + Math.log(level)))
    );
  }

  return data;
},
function fn3(levelMax) {
  const data = ['Math.round(level * Math.log(level))'];

  for (let level = 1; level <= levelMax; level++) {
    data.push(
      Math.round(level * Math.log(level))
    );
  }

  return data;
}
];

export default class StatsPanel extends PureComponent {

  componentDidMount() {
    c3.generate({
      bindto: '#chart',
      data: {
        columns: funcs.map((fn) => (fn(200)))
      },
      axis: {
        y: {
          label: { // ADD
            text: 'monsters',
            position: 'outer-middle'
          }
        }
      }
    });
  }

  render() {
    return (
      <div className={style.container}>
        <div id="chart" style={{ width: '500px', height: '500px' }} />
      </div>
    );
  }
}
