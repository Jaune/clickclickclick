import React from 'react';
import computeRepairCosts from '../lib/computeRepairCosts';
import style from './RepairButton.css';

export default function RepairButton({ state, onClick }) {
  const costs = computeRepairCosts(state);

  return (
    <div className={style.container}>
      <div className={style.costs}>
        <div className={[style.cost, style.gold].join(' ')}>{(costs && costs.gold) || 0}</div>
      </div>
      <button onClick={onClick} disabled={!costs} className={style.button}>
        Repair !
      </button>
    </div>
  );
}
