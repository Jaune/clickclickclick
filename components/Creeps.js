import React from 'react';
import MiniGauge from './MiniGauge';
import style from './Creeps.css';


export default function Creeps({ creeps }) {
  // console.log(wave.classes);

  return (
    <div className={style.creeps}>
      {Object.keys(creeps).map(function (key) {
        const creep = creeps[key];
        const d = creep.distance / 1000;

        const a = (Math.PI * 2) * creep.alpha;
        const r = 6.5 + (15 * d);

        const x = -0.5 + (Math.cos(a) * r) ;
        const y = -0.5 + (Math.sin(a) * r);

        const classes = [style.creep];

        if (d <= 0) {
          classes.push(style.fight);
        }

        if (creep.loot > 0) {
          classes.push(style.loot);
        }

        return (
          <div key={creep.key}
               className={classes.join(' ')}
               style={{ transform: `translate(${x}rem, ${y}rem) scale(${creep.scale})` }}>
            {(creep.hp === creep.hpMax) ? null : <MiniGauge value={creep.hp} max={creep.hpMax} />}
          </div>
        )
      })}
    </div>
  );
}
