import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import style from './Application.css';
import Gauge from './Gauge';
import Gold from './Gold';
import Kill from './Kill';
import Corpses from './Corpses';
import Creeps from './Creeps';
import Generator from './Generator';
import RepairButton from './RepairButton';
import generators from '../generators';

function Application(props) {

  const gens = Object.keys(generators).map(function (identifier) {
    return <Generator
              key={identifier}
              identifier={identifier}
              onBuy={() => (props.buyGenerator(identifier))}
              count={props.game.generators[identifier].count} />
  });

  return (
    <div className={style.container}>
      <div className={style.castle}>
        <Gauge value={props.game.castle.hp} max={props.game.castle.hpMax} />
        <div className={style.castleImage}>Castle</div>
      </div>
      <Corpses corpses={props.game.corpses} />
      <Creeps creeps={props.game.wave.creeps} />


      <Gold value={props.game.gold} />
      <Kill value={props.game.kill} />

      <div className={style.wave}>
        <div className={style.waveImage}>Monsters</div>
        <div className={style.waveLevel}>{props.game.wave.level}</div>
        <Gauge value={props.game.wave.hp} max={props.game.wave.hpMax} />
      </div>

      <div className={style.actions}>
        <button onClick={props.attack} className={style.action}>Attack !</button>
        <RepairButton state={props.game} onClick={props.repair} />
      </div>

      <div className={style.generators}>
        {gens}
      </div>
    </div>
  );
}

function mapStateToProps(state) {
  return {
    game: state.game
  };
}

import debounce from 'debounce';
import attack from '../actions/attack';
import repair from '../actions/repair';
import buyGenerator from '../actions/buyGenerator';

function mapDispatchToProps(dispatch) {
  const creators = bindActionCreators({
    attack,
    repair,
    buyGenerator
  }, dispatch);

  creators.attack = debounce(creators.attack, 111, true);

  return creators;
}

export default connect(mapStateToProps, mapDispatchToProps)(Application);
