const DISTANCE_MIN = 1000;
export const DISTANCE_MAX = 1250;
const DISTANCE_DIFF = Math.max(0, DISTANCE_MAX - DISTANCE_MIN);

const CREEP_COUNT_MIN = 8;
const CREEP_COUNT_MAX = 16;
const CREEP_COUNT_DIFF = Math.max(0, CREEP_COUNT_MAX - CREEP_COUNT_MIN);

const CREEP_SCALE_MIN = 1;
const CREEP_SCALE_MAX = 2;
const CREEP_SCALE_DIF = Math.max(0, CREEP_SCALE_MAX - CREEP_SCALE_MIN);


function computeHpMax(level) {
  return Math.max(1, Math.round(level * Math.log(level)));
}

export default function makeWave(level) {
  const creepByClass = {};
  const creepCountMax = Math.round(CREEP_COUNT_MIN + (Math.random() * CREEP_COUNT_DIFF));
  const hpWave = computeHpMax(level);
  let creeps = [];
  let hpPool = hpWave;
  const hpAverage = Math.max(1, Math.floor(hpWave / creepCountMax));

  while (hpPool > 0) {
    let hp = hpAverage;

    hpPool -= hp;

    if (hpPool < hpAverage) {
      hp += hpPool;
      hpPool = 0;
    }

    creepByClass[hp] = !creepByClass[hp] ? 1 : creepByClass[hp] + 1;

    creeps.push({
      key: [
        level,
        creeps.length,
        Math.round(Math.random() * 0xff00ff)
      ].map((n)=>(n.toString(36))).join('-'),
      distance: Math.round(DISTANCE_MIN + (Math.random() * DISTANCE_DIFF)),
      hp,
      phase: 'attack',
      scale: CREEP_SCALE_MIN,
      hpMax: hp,
      damagePerAttack: hp,
      lootPerAttack: hp,
      lootCapacity: hp,
      loot: 0,
      alpha: Math.random()
    });
  }

  const classes = Object.keys(creepByClass)
    .map((k)=>(parseInt(k)))
  ;

  classes.sort((a, b) => (a - b));

  creeps = creeps.map(function (c) {
    const r = classes.indexOf(c.hpMax) / classes.length;

    c.scale = CREEP_SCALE_MIN + (r * CREEP_SCALE_DIF);

    return c;
  });

  return {
    level: level,
    hp: hpWave,
    hpMax: hpWave,
    creeps: creeps.reduce(function (all, c) { all[c.key] = c; return all; }, {}),
    distancePerTick: 100
  };
}
