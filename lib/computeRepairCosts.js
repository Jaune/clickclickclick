export default function computeRepairCosts(state){
  const { hp, hpMax } = state.castle;
  const { gold } = state;

  if (hp < hpMax) {
    let repairNeeded = hpMax - hp;
    let repairCost = Math.ceil(repairNeeded / 100);
    let goldLeft = gold - repairCost;
    let pay = goldLeft <= 0 ? gold : repairCost;

    return { gold: pay }
  }

  return null;
}
