import GENERATORS from '../generators';

export default function computeCosts(ident, count) {
  const G = GENERATORS[ident];

  return {
    gold: Math.round(G.gold.base * Math.pow(G.gold.growthRate, count))
  };
}
