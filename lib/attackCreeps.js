import makeWave from './makeWave';

function immutableDelete(arr, index) {
   return arr.slice(0,index).concat(arr.slice(index+1))
}

function transformCreepToCorpse(creep) {
  return Object.assign({}, creep, {
    ttl: Math.round(8 + (Math.random() * 8))
  });
}

function sortCreeps(state) {
  const creeps = Object.keys(state.wave.creeps).reduce(function (all, key) {
    all.push(state.wave.creeps[key]);

    return all;
  }, []);

  creeps.sort((a, b) => {
    if (a.distance === b.distance) {
      return a.hp - b.hp;
    }
    return a.distance - b.distance;
  });

  return creeps;
}

export default function attackCreeps(state, hit, damage) {
  let gold = state.gold;
  let kill = state.kill;
  let creepsArray = sortCreeps(state);
  let corpses = state.corpses;
  let creeps = Object.assign({}, state.wave.creeps);

  for (let i = 0, l = hit; i < l; i++) {
    const index = i % creepsArray.length;
    const creep = creepsArray[index];
    const hp = creep.hp - damage;

    if (hp <= 0) {
      corpses = [...corpses, transformCreepToCorpse(creep)];
      delete creeps[creep.key];
      creepsArray = immutableDelete(creepsArray, index);
      kill++;
      gold += creep.hpMax + creep.loot;
    } else {
      creeps[creep.key] = Object.assign({}, creep, { hp });
    }
  }

  const waveHp = Object.keys(creeps).reduce((sum, key) => (sum + creeps[key].hp), 0);

  const wave = (waveHp <= 0)
    ? makeWave(state.wave.level + 1)
    : Object.assign({}, state.wave, { hp: waveHp, creeps })
    ;

  return Object.assign({}, state, {
    gold,
    kill,
    wave,
    corpses
  });
}
