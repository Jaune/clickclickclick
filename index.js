import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import reducers from './reducers'
import Application from './components/Application'

let store = createStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())

import tick from './actions/tick.js';

window.__INTERVAL__ = setInterval(function () {
  store.dispatch(tick());
}, 1000);

ReactDOM.render(
  <Provider store={store}>
    <Application />
  </Provider>,
  document.querySelector('[data-root]')
)
