import makeWave, { DISTANCE_MAX } from '../../lib/makeWave';
import attackCreeps from '../../lib/attackCreeps';
import GENERATORS from '../../generators';

function moveCreeps(state) {
  const creeps = {};

  Object.keys(state.wave.creeps).forEach(function (key) {
    const creep = state.wave.creeps[key];
    let distance = creep.distance;

    if (creep.phase === 'attack') {
      distance = Math.max(0, creep.distance - state.wave.distancePerTick);
    } else if (creep.phase === 'retreat') {
      distance = Math.max(0, creep.distance + state.wave.distancePerTick);
    }

    if (distance <= DISTANCE_MAX) {
      creeps[key] = Object.assign({}, creep, {
        distance
      });
    }
  });

  if (Object.keys(creeps).length === 0) {
    return Object.assign({}, state, {
      wave: makeWave(state.wave.level)
    });
  }

  return Object.assign({}, state, {
    wave: Object.assign({}, state.wave, { creeps })
  });
}

function attackCastle(state) {
  const creeps = state.wave.creeps;
  let hp = state.castle.hp;
  let gold = state.gold;
  let newWave = state.wave;

  Object.keys(creeps).forEach(function (key) {
    const creep = creeps[key];

    if (creep.distance <= 0) {
      hp =  Math.max(0, hp - creep.damagePerAttack);

      if (hp <= 0) {
        const lootPerAttack = Math.min(gold, creep.lootPerAttack, creep.lootCapacity - creep.loot);

        gold = Math.max(0, gold - lootPerAttack);
        const loot = creep.loot + lootPerAttack;

        const update = { loot };

        if (gold <= 0 || loot >= creep.lootCapacity) {
          update.phase = 'retreat';
        }

        newWave = Object.assign({}, newWave, {
          creeps: Object.assign({}, creeps, {
            [key]: Object.assign({}, creeps[key], update)
          })
        });
      }
    }
  });

  return Object.assign({}, state, {
    gold,
    castle:  Object.assign({}, state.castle, { hp }),
    wave: newWave
  });
}

const MAX_CORPSES = 55;

function collectCorpses(state) {
  const newCorpses = state.corpses
    .map(function (c) {
      return Object.assign({}, c, {
        ttl: c.ttl - 1
      });
    })
    .filter(function (c) {
      return c.ttl > 0;
    })
  ;

  if (newCorpses.length > MAX_CORPSES) {
    for (let i = 0, l = Math.floor(newCorpses.length / 2); i < l; i++) {
      newCorpses[i].ttl = Math.round(1 + Math.random());
    }
  }

  return Object.assign({}, state, {
    corpses: newCorpses
  });
}


export default function doTick(state, action) {
  Object.keys(state.generators).forEach(function (ident) {
    const hitPerAttack = state.generators[ident].count * GENERATORS[ident].hitPerAttack;

    if  (hitPerAttack > 0) {
      state = attackCreeps(state, hitPerAttack, GENERATORS[ident].damagePerAttack);
    }
  });

  state = attackCastle(state)
  state = moveCreeps(state);

  state = collectCorpses(state);

  return state;
}
