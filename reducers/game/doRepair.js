import computeRepairCosts from '../../lib/computeRepairCosts';

export default function doRepair(state, action) {
  const costs = computeRepairCosts(state);
  const { hp, hpMax } = state.castle;
  const { gold } = state;

  if (costs) {
    return Object.assign({}, state, {
      gold: Math.max(0, gold - costs.gold),
      castle: Object.assign({}, state.castle, {
        hp: Math.min(hp + (costs.gold * 100), hpMax)
      })
    });
  }

  return state;
}
