import attackCreeps from '../../lib/attackCreeps';

export default function doAttack(state, action) {
  return attackCreeps(state, state.crown.hitPerAttack, state.crown.damagePerAttack);
}
