import makeWave from '../../lib/makeWave';
import generators from '../../generators';

const initialState = {
  gold: 100,
  kill: 0,
  corpses: [],
  castle: {
    hp: 1000,
    hpMax: 1000
  },
  crown: {
    damagePerAttack: 1,
    hitPerAttack: 1
  },
  generators: Object.keys(generators).reduce(function (all, indent) {
    all[indent] = {
      count: 0
    };
    return all;
  }, {}),
  wave: makeWave(1)
};

import doAttack from './doAttack';
import doTick from './doTick';
import doBuyGenerator from './doBuyGenerator';
import doRepair from './doRepair';

export default function (state = initialState, action) {
  switch (action.type) {
    case 'ATTACK':
        return doAttack(state, action);
    case 'TICK':
        return doTick(state, action);
    case 'BUY_GENERATOR':
        return doBuyGenerator(state, action);
    case 'REPAIR':
        return doRepair(state, action);
  }

  return state;
}
