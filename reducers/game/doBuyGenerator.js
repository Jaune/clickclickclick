import computeCosts from '../../lib/computeCosts';

export default function doBuyGenerator(state, action) {
  const ident = action.identifier;
  const generator = state.generators[ident];
  let gold = state.gold;

  const costs = computeCosts(ident, generator.count);


  if (gold > costs.gold) {
    gold -= costs.gold;

    const newGenerator = Object.assign({}, state.generators[ident], {
      count: generator.count + 1
    });

    const newGenerators = Object.assign({}, state.generators, {
      [ident]: newGenerator
    });

    return Object.assign({}, state, {
      gold,
      generators: newGenerators
    });
  }

  return state;
}
