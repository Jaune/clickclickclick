const resolve = require("path").resolve;
const HtmlWebpackPlugin = require('html-webpack-plugin');
const LoaderOptionsPlugin = require('webpack/lib/LoaderOptionsPlugin');
const DefinePlugin = require('webpack/lib/DefinePlugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJsPlugin = require('webpack/lib/optimize/UglifyJsPlugin');

module.exports = {
  entry: {
    main: resolve(__dirname, '..', 'index.js')
  },
  output: {
    path: resolve(__dirname, '..', 'public'),
    publicPath: '/clickclickclick',
    filename: '[name].[hash].js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: {
            loader: 'css-loader',
            options: {
              modules: true,
              minimize: true,
              localIdentName: '[name]__[local]--[hash:base64:5]'
            }
          }
        })
     },
     {
      test: /\.css$/,
      include: /node_modules/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [
          'css-loader'
        ]
      })
    },
     {
       test: /\.svg/,
       use: {
         loader: 'svg-url-loader',
         options: {}
       }
     }
   ]
  },
  plugins: [
    new LoaderOptionsPlugin({
      minimize: true,
      debug: false
    }),
    new UglifyJsPlugin({
      beautify: false,
      mangle: {
        screw_ie8: true,
        keep_fnames: true
      },
      compress: {
        screw_ie8: true
      },
      comments: false
    }),
    new DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production')
    }),
    new ExtractTextPlugin({
      filename: 'style.[hash].css',
      allChunks: true
    }),
    new HtmlWebpackPlugin({
      title: 'ClickClickClick',
      googleAnalyticsID: 'UA-102260488-2',
      assets: {
        "style"  : "style.[hash].css",
      },
      template: resolve(__dirname, 'template.ejs')
    })
  ]
};
