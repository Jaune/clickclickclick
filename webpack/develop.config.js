const resolve = require("path").resolve;
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    main: resolve(__dirname, '..', 'index.js')
  },
  devtool: 'cheap-source-map',
  output: {
    path: resolve(__dirname, '..', 'dist'),
    publicPath: '/',
    filename: '[name].bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
       test: /\.css$/,
       exclude: /node_modules/,
       use: [
         'style-loader',
         {
           loader: 'css-loader',
           options: {
             modules: true,
             localIdentName: '[name]__[local]--[hash:base64:5]'
           }
         }
       ]
     },
     {
      test: /\.css$/,
      include: /node_modules/,
      use: [
        'style-loader',
        'css-loader'
      ]
    },
     {
       test: /\.svg/,
       use: {
         loader: 'svg-url-loader',
         options: {}
       }
     }
   ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'ClickClickClick',
      template: resolve(__dirname, 'template.ejs')
    })
  ],
  devServer: {
    host: '0.0.0.0',
    allowedHosts: ['93.26.183.248'],
    inline: false,
    hot: false,
    compress: true,
    port: 3080
  }
};
