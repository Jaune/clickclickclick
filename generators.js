export default {
  'swordsman': {
    hitPerAttack: 1,
    damagePerAttack: 1,
    gold: {
      base: 150,
      growthRate: 1.07
    }
  },
  'archer': {
    hitPerAttack: 1,
    damagePerAttack: 2,
    gold: {
      base: 280,
      growthRate: 1.08
    }
  },
  'ice-mage': {
    hitPerAttack: 1,
    damagePerAttack: 4,
    gold: {
      base: 580,
      growthRate: 1.09
    }
  },
  'fire-mage': {
    hitPerAttack: 1,
    damagePerAttack: 6,
    gold: {
      base: 880,
      growthRate: 1.10
    }
  }
};
